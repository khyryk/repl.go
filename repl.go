package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"regexp"
	"strings"
)

type REPL struct {
	//counts for matching blocks, multiline strings, etc
	brackets   int
	braces     int
	parens     int
	backquotes int

	isImport bool
	isType   bool
	isFunc   bool

	//keep track of unused imports
	imports map[string]bool

	//keep track of unused vars
	vars map[string]bool

	//skeleton source code to be added to
	source string
}

// Counts brackets, parens, and backquotes in the source to determine whether the input is multiline.
func (r *REPL) isMultiline() bool {
	openingBrackets := strings.Count(r.source, "[")
	closingBrackets := strings.Count(r.source, "]")
	r.brackets = openingBrackets - closingBrackets

	openingBraces := strings.Count(r.source, "{")
	closingBraces := strings.Count(r.source, "}")
	r.braces = openingBraces - closingBraces

	openingParens := strings.Count(r.source, "(")
	closingParens := strings.Count(r.source, ")")
	r.parens = openingParens - closingParens

	r.backquotes = strings.Count(r.source, "`")

	return r.brackets > 0 || r.braces > 0 || r.parens > 0 || r.backquotes%2 == 1
}

// Given code received from input, decide where and how to add the code to the present source code.
func (r *REPL) addCode(code string) {
	if strings.Contains(code, "import ") && strings.Index(code, "import ") == 0 || r.isImport {
		r.source = strings.Replace(r.source, "//import", code+"//import", 1)
		r.isImport = r.isMultiline()
	} else if strings.Contains(code, "type ") && strings.Index(code, "type ") == 0 || r.isType {
		r.source = strings.Replace(r.source, "//types", code+"//types", 1)
		r.isType = r.isMultiline()
	} else if strings.Contains(code, "func ") && strings.Index(code, "func ") == 0 || r.isFunc {
		r.source = strings.Replace(r.source, "//funcs", code+"//funcs", 1)
		r.isFunc = r.isMultiline()
	} else {
		r.source = strings.Replace(r.source, "//main", code+"//main", 1)
	}
}

// Removes funcs that shouldn't keep re-running upon each exec, such as println.
// Note: Unused variable handing should be implemented before this is fully used.
func (r *REPL) removeOnceThroughFuncs() {
	rePrintln := regexp.MustCompile("((fmt\\.P)|p)rintln\\(.*\\)\\s*;*")
	r.source = rePrintln.ReplaceAllLiteralString(r.source, "")
}

func main() {
	//skeleton source code to be added to
	previousSource :=
		`package main

//import

//types

//funcs

func main() {
//main
}`

	repl := new(REPL)
	repl.source = previousSource

	for {
		fmt.Printf("> ")

		//modify the source as needed before allowing it to be executed
		for {
			reader := bufio.NewReader(os.Stdin)
			input, _ := reader.ReadString('\n')
			repl.addCode(input)
			if !repl.isMultiline() {
				break
			}
		}

		path := os.TempDir() + "/repltempsource.go"
		err := ioutil.WriteFile(path, []byte(repl.source), 0644)
		if err != nil {
			panic(err)
		}

		out, err := exec.Command("go", "run", path).CombinedOutput()
		//if there's an error, revert to previousSource; else, back up current source
		if err != nil {
			//fmt.Println(err.Error())
			repl.source = previousSource
		} else {
			previousSource = repl.source
		}

		repl.removeOnceThroughFuncs()
		fmt.Println(string(out))
	}
}
